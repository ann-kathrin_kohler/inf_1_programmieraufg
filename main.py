# --------------------Licence ---------------------------------------------
# Copyright (c) <2022>
# - A. Kohler (ann-kathrin.kohler@student.jade-hs.de) and
# - L. Stoffel (leon.stoffel@student.jade-hs.de)
# Jade University of Applied Sciences
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject
# to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------


"""This module contains the main functions for user interaction and control logic."""


import datetime
import os
from textwrap import dedent

import board
import meeplemaze
import movements
import tracking

GAME_MODE_DEFAULT = 1
GAME_MODE_REPLAY = 2


def main():
    """Display the main menu and let the user choose the game mode.
       Afterwards, solve a new maze or replay an older one depending on the user choice."""
    game_mode = display_main_menu()

    if game_mode == GAME_MODE_DEFAULT:
        game = create_game_from_user_input()

        timestamp_for_filename = datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        map_filename = f'map_{timestamp_for_filename}'
        tracking_path_filename = f'tracking_path_{timestamp_for_filename}'
        tracking_path = []

        game.saveCurrentMap(map_filename)
        solve_maze(game, tracking_path)

        tracking.save_tracking_path_to_file(tracking_path, tracking_path_filename)
        game.finish()

    elif game_mode == GAME_MODE_REPLAY:
        game, tracking_path = load_game_and_tracking_path_for_replay()

        tracking.move_along_tracking_path(game, tracking_path)
        game.finish()


def display_main_menu():
    """Displays the main menu and let the user select which of the two options he/she want's to choose:

       Mode 1:  The first option is to create a board and start the game.
       Mode 2:  The second option is to provide a file_name for an already solved map (and tracking path)
                and watch the replay according to the movements in the tracking path."""
    while True:
        try:
            print_dedent("""
                Hello and welcome to MeepleMaze :)
                At first you have to decide which mode you want to play.
        
                You have two options to choose from:
                The first option is to create a board and start the game.
                The second option is to provide a file_name for an already played map (and tracking path),
                then I will move and collect boxes like according to the movements in the tracking path.
                If you want to play option one, type "1":
                If you want to play option two, type "2":""")

            choice = int(input())
            if choice not in [1, 2]:
                print('Input error, please try again.')
                continue

            return choice
        except ValueError:
            print('Input error, please try again.')
            continue


def print_dedent(text):
    """Print the text without any indention."""
    print(dedent(text))


def create_game_from_user_input():
    """Ask the user for length of axes, display speed and seed value (specific game board or random choice)
       and return the game board created according to the input values."""
    while True:
        try:
            print_dedent("""
                    Good choice! Now we have to create a board.
                    If you want to use default values (size = 8), press [Enter].""")

            len_x_axis = input('Please choose the length of the x-axis, min: 3 ')
            len_y_axis = input('Please choose the length of the y-axis, min: 3 ')
            if len_x_axis == '' and len_y_axis == '':
                size = [8, 8]
            elif int(len_x_axis) >= 3 and int(len_y_axis) >= 3:
                size = [int(len_x_axis), int(len_y_axis)]
            else:
                print('Input error, please try again.')
                continue

            print('How fast should the game run? (10 = slow, 500 = extreme fast) ')
            rate_input = input('If you want to use the default speed (50), press [Enter]. ')
            if rate_input == '':
                rate = 50
            elif int(rate_input) > 0:
                rate = int(rate_input)
            else:
                print('Input error, please try again.')
                continue

            seed_input = input("Which board do you want to play? Input a number, otherwise I'll take a random board: ")
            if seed_input == '':
                seed = -1
            elif int(seed_input) > 0:
                seed = int(seed_input)
            else:
                print('Input error, please try again.')
                continue

        except ValueError:
            print('Input error, please try again.')
            continue

        game = meeplemaze.MeepleMaze("random", size, seed, rate, callFinishAutomatically=False)
        return game


def solve_maze(game, tracking_path):
    """Solve the maze by collecting all blue boxes on the game board and record the movements in 'tracking_path'."""
    while board.are_boxes_left(game):
        num_boxes_east = board.get_num_boxes_east(game)
        num_boxes_west = board.get_num_boxes_west(game)
        num_boxes_north = board.get_num_boxes_north(game)
        num_boxes_south = board.get_num_boxes_south(game)

        if num_boxes_east > 0:
            distance = board.distance_to_next_box_east(game)
            movements.move_east(game, distance - 1, tracking_path)
            movements.collect_east(game, num_boxes_east, tracking_path)
        elif num_boxes_west > 0:
            distance = board.distance_to_next_box_west(game)
            movements.move_west(game, distance - 1, tracking_path)
            movements.collect_west(game, num_boxes_west, tracking_path)
        elif num_boxes_north > 0:
            distance = board.distance_to_next_box_north(game)
            movements.move_north(game, distance - 1, tracking_path)
            movements.collect_north(game, num_boxes_north, tracking_path)
        elif num_boxes_south > 0:
            distance = board.distance_to_next_box_south(game)
            movements.move_south(game, distance - 1, tracking_path)
            movements.collect_south(game, num_boxes_south, tracking_path)
        else:
            movements.walk_random(game, tracking_path)


def load_game_and_tracking_path_for_replay():
    """Load a game board and corresponding tracking path from file name (if both exist)
       and return the game board and the tracking path."""
    while True:
        print_dedent("""
                Good choice! Now we have to look for a file with an already solved map.
                Tell me the name of your favorite map file and ensure,
                that the map and tracking_path file are in the current directory.
                Furthermore, ensure that the tracking path file has the same date suffix as the map file,
                e.g. 'map_18_12_2021_18_36_58' and 'tracking_path_18_12_2021_18_36_58'""")

        map_name = input('For example: map_18_12_2021_18_36_58 : \n')
        date_suffix = map_name[-19:]
        tracking_path_name = f'tracking_path_{date_suffix}'

        if not os.path.isfile(map_name):
            print('Invalid map name, please try again.')
            continue

        if not os.path.isfile(tracking_path_name):
            print('Tracking path for map not found (using name convention), please try again.')
            continue

        game = meeplemaze.MeepleMaze(map_name, rate=50, callFinishAutomatically=False)
        tracking_path = tracking.load_tracking_path_from_file(tracking_path_name)
        return game, tracking_path


if __name__ == '__main__':
    main()
