# MeepleMaze

## Voraussetzungen
-	Python 3.9 oder aktueller
-	MeepleMaze benötigt VPython (pip install vpython)

## Erforderliche Dateien
- meeplemaze.py (Ver 1.3 zur Verfügung gestellt von S. Franz)
- board.py
- movements.py
- tracking.py
- main.py

### Optional
- Map und passende tracking_path-Dateien mit Zeitstempel im Dateinamen
  (zB: map_16_01_2022_17_52_12 und tracking_path_16_01_2022_17_52_12)

## Programmablauf
Zum Starten des Programms muss die Datei main.py ausgeführt werden. 
Der Benutzer kann über das Terminal mittels Eingabeparametern den gewünschten Spielmodus wählen. 

Spielmodus 1: Der Benutzer erstellt ein Feld oder nutzt alternativ die gegebenen Standardwerte.
MeepleMaze startet und sammelt die Boxen auf dem Feld ein, bis alle Boxen eingesammelt wurden. 
Das Spiel speichert das Feld und den gelaufenen Pfad und beendet sich automatisch.

Spielmodus 2: Der Benutzer kann den Namen (Namenskonventionen: siehe oben) 
einer im aktuellen Verzeichnis vorhandenen map-Datei eingeben 
und MeepleMaze sucht die passende tracking_path Datei heraus.
MeepleMaze startet automatisch und läuft auf dem ausgewählten Spielfeld den Weg 
entsprechend den Anweisungen der tracking_path-Datei. 
Anschließend beendet sich das Spiel automatisch.