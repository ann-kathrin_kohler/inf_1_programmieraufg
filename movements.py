# --------------------Licence ---------------------------------------------
# Copyright (c) <2022>
# - A. Kohler (ann-kathrin.kohler@student.jade-hs.de) and
# - L. Stoffel (leon.stoffel@student.jade-hs.de)
# Jade University of Applied Sciences
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject
# to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------


"""This module contains various functions to move and collect boxes as well as
   checking directions and perform random movements to find the remaining boxes on the board."""


import random


def move_east(game, steps, tracking_path=None):
    """Move on positive x-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveX(steps, False)

    if tracking_path is not None:
        breadcrumb = 'ME' + str(steps)
        tracking_path.append(breadcrumb)


def move_west(game, steps, tracking_path=None):
    """Move on negative x-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveX(-steps, False)

    if tracking_path is not None:
        breadcrumb = 'MW' + str(steps)
        tracking_path.append(breadcrumb)


def move_north(game, steps, tracking_path=None):
    """Move on positive y-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveY(steps, False)

    if tracking_path is not None:
        breadcrumb = 'MN' + str(steps)
        tracking_path.append(breadcrumb)


def move_south(game, steps, tracking_path=None):
    """Move on negative y-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveY(-steps, False)

    if tracking_path is not None:
        breadcrumb = 'MS' + str(steps)
        tracking_path.append(breadcrumb)


def collect_east(game, steps, tracking_path=None):
    """Move and collect boxes on positive x-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveX(steps, True)

    if tracking_path is not None:
        breadcrumb = 'CE' + str(steps)
        tracking_path.append(breadcrumb)


def collect_west(game, steps, tracking_path=None):
    """Move and collect boxes on negative x-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveX(-steps, True)

    if tracking_path is not None:
        breadcrumb = 'CW' + str(steps)
        tracking_path.append(breadcrumb)


def collect_north(game, steps, tracking_path=None):
    """Move and collect boxes on positive y-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveY(steps, True)

    if tracking_path is not None:
        breadcrumb = 'CN' + str(steps)
        tracking_path.append(breadcrumb)


def collect_south(game, steps, tracking_path=None):
    """Move and collect boxes on negative y-axis for a given number of 'steps'
       and add this movement to the 'tracking_path' (if one is provided)."""
    game.moveY(-steps, True)

    if tracking_path is not None:
        breadcrumb = 'CS' + str(steps)
        tracking_path.append(breadcrumb)


def is_opposite_direction(new_direction, tracking_path=None):
    """Checks if the given 'new_direction' is the 'opposite_direction' of the 'last_movement' in the 'tracking_path'
       to prevent Meeple from moving back and forth over and over again."""
    if not tracking_path:
        old_direction = 'N'
    else:
        last_movement = tracking_path[-1]
        old_direction = last_movement[1:2]

    dictionary_opposite_directions = {
        'E': 'west',
        'N': 'south',
        'W': 'east',
        'S': 'north'
    }

    opposite_direction = dictionary_opposite_directions[old_direction]
    return new_direction == opposite_direction


def walk_random(game, tracking_path):
    """Roll a dice for direction and length for a random movement.
       If the direction is opposite to the old direction roll the dice once again
       to prevent Meeple from moving back and forth over and over again."""
    directions = ['east', 'north', 'west', 'south']
    random_value_for_direction = random.randint(0, 3)
    random_direction = directions[random_value_for_direction]

    while is_opposite_direction(random_direction, tracking_path):
        random_value_for_direction = random.randint(0, 3)
        random_direction = directions[random_value_for_direction]

    len_x_axis, len_y_axis = game.getDimension()
    len_axis = max(len_x_axis, len_y_axis)

    x_position, y_position = game.getPosition()

    if random_direction == 'east' or random_direction == 'west':
        random_len_for_route = random.randint(1, len_axis - x_position)
    else:
        random_len_for_route = random.randint(1, len_axis - y_position)

    if random_direction == 'east':
        move_east(game, random_len_for_route, tracking_path)
    elif random_direction == 'north':
        move_north(game, random_len_for_route, tracking_path)
    elif random_direction == 'west':
        move_west(game, random_len_for_route, tracking_path)
    elif random_direction == 'south':
        move_south(game, random_len_for_route, tracking_path)
