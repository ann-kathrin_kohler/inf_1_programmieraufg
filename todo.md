
# TODO:

##       * Urheber-Kram: 
           - Alle Quellcode Dateien enthalten Informationen zur Urheberschaft.
           - wenn ok dann in jede Datei

##       * Read-Me:
           - Dem Programm liegt eine Readme Datei bei, die kurz die Kompilierung, den Start 
             (inkl. Parametern) sowie die Bedienung des Programms erläutert.
               -> alle Input Parameter aufzählen?!?

##       * Kommentieren
           Der Quellcode ist sinnvoll und ausführlich in Englisch(!) kommentiert.
           Funktionen/Methoden/Klassen sind ausführlich mit Ein und Ausgabeparameter beschrieben.
              -> copy/paste bei Kompass

##       * Abgabe:
           - Die Abgabe erfolgt als gezipptes Archiv (nicht rar, nicht 7zip, ...).
           - Das Archiv hat den Dateinamen inf1_programmieraufgabe_grpNN.zip, wobei NN ihre
             Gruppennummer ist (nicht *.zip.zip)!
           - Es befinden sich keine Projektdateien (pyCharm, Eclipse, etc.) oder sonstige nicht
             unmittelbar zur  Nutzung des Programms notwendigen Dateien in dem Archiv.
                   -> Bsp Maps, oder schwere Map einfügen?
   
