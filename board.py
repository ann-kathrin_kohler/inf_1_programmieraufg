# --------------------Licence ---------------------------------------------
# Copyright (c) <2022>
# - A. Kohler (ann-kathrin.kohler@student.jade-hs.de) and
# - L. Stoffel (leon.stoffel@student.jade-hs.de)
# Jade University of Applied Sciences
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject
# to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------


"""This module provides various functions to retrieve information
   about board dimensions, number of boxes and their distance."""


def are_boxes_left(game):
    """Check if there are still boxes left on the 'game' board."""
    return game.getNumOfBlueBoxes() > 0


def get_dimensions(game):
    """Get the dimensions of the board game as length for x- and y-axis."""
    len_x_axis, len_y_axis = game.getDimension()
    return len_x_axis, len_y_axis


def get_num_boxes_east(game):
    """Get the number of boxes left on positive part of the x-axis."""
    len_x_axis, _ = game.getDimension()
    board = game.getFieldValueX(len_x_axis)
    number_of_boxes = get_num_boxes_on_axis(board)
    return number_of_boxes


def get_num_boxes_west(game):
    """Get the number of boxes left on negative part of the x-axis."""
    len_x_axis, _ = game.getDimension()
    board = game.getFieldValueX(-len_x_axis)
    number_of_boxes = get_num_boxes_on_axis(board)
    return number_of_boxes


def get_num_boxes_north(game):
    """Get the number of boxes left on positive part of the y-axis."""
    _, len_y_axis = game.getDimension()
    board = game.getFieldValueY(len_y_axis)
    number_of_boxes = get_num_boxes_on_axis(board)
    return number_of_boxes


def get_num_boxes_south(game):
    """Get the number of boxes left on negative part of the y-axis."""
    _, len_y_axis = game.getDimension()
    board = game.getFieldValueY(-len_y_axis)
    number_of_boxes = get_num_boxes_on_axis(board)
    return number_of_boxes


def get_num_boxes_on_axis(board_values):
    """Get the number of consecutive boxes for a given axis provided as 'board_values'."""
    number_of_fields = len(board_values)
    num_boxes = 0
    i = 0
    while i < number_of_fields:
        if board_values[i] == 1:
            while board_values[i] == 1:
                num_boxes += 1
                i += 1
            break
        else:
            i += 1
    return num_boxes


def distance_to_next_box_east(game):
    """Get the distance to the next box on positive part of the x-axis."""
    len_x_axis, _ = game.getDimension()
    board_values = game.getFieldValueX(len_x_axis)
    distance = distance_to_next_box(board_values)
    return distance


def distance_to_next_box_west(game):
    """Get the distance to the next box on negative part of the x-axis."""
    len_x_axis, _ = game.getDimension()
    board_values = game.getFieldValueX(-len_x_axis)
    distance = distance_to_next_box(board_values)
    return distance


def distance_to_next_box_north(game):
    """Get the distance to the next box on positive part of the y-axis."""
    _, len_y_axis = game.getDimension()
    board_values = game.getFieldValueY(len_y_axis)
    distance = distance_to_next_box(board_values)
    return distance


def distance_to_next_box_south(game):
    """Get the distance to the next box on negative part of the y-axis."""
    _, len_y_axis = game.getDimension()
    board_values = game.getFieldValueY(-len_y_axis)
    distance = distance_to_next_box(board_values)
    return distance


def distance_to_next_box(board_values):
    """Get the distance to the next box bases on the 'board_values' provided."""
    distance = 0
    for field in board_values:
        distance += 1
        if field == 1:
            break
    return distance

