# --------------------Licence ---------------------------------------------
# Copyright (c) <2022>
# - A. Kohler (ann-kathrin.kohler@student.jade-hs.de) and
# - L. Stoffel (leon.stoffel@student.jade-hs.de)
# Jade University of Applied Sciences
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject
# to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------

import datetime
from textwrap import dedent

import board
from main import GAME_MODE_DEFAULT
import movements
import tracking
import solver


GAME_MODE_DEFAULT = 1
GAME_MODE_REPLAY = 2


def main():
    """Check out the game mode by using the 'read_user_input' method:

       playmode 1: creating a map- and 'tracking_path'-filename incl. time stemp, move over a created board,
       search and collect all boxes, using movements and board methods, save the 'tracking_path' in a file,
       using 'save_tracking_path_to_file' method

       playmode 2: load the coordinates from a file into the 'tracking_path'-list, using the 
       'load_tracking_path_from_file' method and move along this path, using the 'move_along_tracking_path' method"""

    play_mode, game, replay_path = solver.read_user_input()


    if play_mode == GAME_MODE_DEFAULT:
        timestamp_for_filename = datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        map_filename = f'map_{timestamp_for_filename}'
        tracking_path_filename = f'tracking_path_{timestamp_for_filename}'
        game.saveCurrentMap(map_filename)

        tracking_path = []

        while board.are_boxes_left(game):
            num_boxes_east = board.get_num_boxes_east(game)
            num_boxes_west = board.get_num_boxes_west(game)
            num_boxes_north = board.get_num_boxes_north(game)
            num_boxes_south = board.get_num_boxes_south(game)

            if num_boxes_east > 0:
                distance = board.distance_to_next_box_east(game)
                movements.move_east(game, distance - 1, tracking_path)
                movements.collect_east(game, num_boxes_east, tracking_path)
            elif num_boxes_west > 0:
                distance = board.distance_to_next_box_west(game)
                movements.move_west(game, distance - 1, tracking_path)
                movements.collect_west(game, num_boxes_west, tracking_path)
            elif num_boxes_north > 0:
                distance = board.distance_to_next_box_north(game)
                movements.move_north(game, distance - 1, tracking_path)
                movements.collect_north(game, num_boxes_north, tracking_path)
            elif num_boxes_south > 0:
                distance = board.distance_to_next_box_south(game)
                movements.move_south(game, distance - 1, tracking_path)
                movements.collect_south(game, num_boxes_south, tracking_path)
            else:
                movements.walk_random(game, tracking_path)

        tracking.save_tracking_path_to_file(tracking_path, tracking_path_filename)
        game.finish()

    elif play_mode == GAME_MODE_REPLAY:
        tracking_path = tracking.load_tracking_path_from_file(replay_path)
        tracking.move_along_tracking_path(game, tracking_path)

    game.finish()


if __name__ == '__main__':
    main()
