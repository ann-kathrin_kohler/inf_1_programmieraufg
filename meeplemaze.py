# MeepleMaze
# Ver. 1.0:     S. Franz & S. Kissner
#   Usage: see README.md
# Ver. 1.1:     S. Franz
#   modified: getFieldValueX(), getFieldValueY(), getFieldValueZ() returns N values in look-direction
# Ver. 1.2:     S. Franz
#   method collect() added
# Ver. 1.3:     S. Franz
#   optional Parameter callFinishAutomatically added in constructor

from vpython import *
from vpython.no_notebook import stop_server
import numpy as np
import time
import random
import json
import os

class MeepleMaze():

    def __init__(self, map, size = [10, 10], seed = -1, rate = 20, callFinishAutomatically = True):
        self.gameField = []                
        self.isRunning = True
        self.moves = 0
        self.collectSwitches = 0
        self.isCollecting = False
        self.callFinishAutomatically = callFinishAutomatically
        self.rate = rate
        self.fields = []
        self.seed = seed
        if self.seed < 0:
            self.seed = int(time.time())
        print("\nUsed seed:\t", self.seed, "\n")
        if not type(size) is list:
            raise ValueError("Size paramter must be Integer or List")
        elif len(size) > 3 or len(size) < 1:
            print("*** Warning: Length of Size paramter must be >= 1 and <= 3! ***")
        if type(size) is int or type(size) is float:
            size = [size]
        if type(map) is str:
            if os.path.isfile(map):
                with open(map, "r") as fp:
                    self.gameField = json.load(fp)
            elif map.lower() == "random":
                if len(size) == 1:
                    self.fieldDimension = [size[0], 1, 1]
                elif len(size) == 2:
                    self.fieldDimension = [size[0], size[1], 1]
                elif len(size) == 3:
                    self.fieldDimension = size.copy()
                random.seed(self.seed)
                for x in range(self.fieldDimension[0]):
                    self.gameField.append([])
                    for y in range(self.fieldDimension[1]):
                        self.gameField[x].append([])
                        for z in range(self.fieldDimension[2]):
                            self.gameField[x][y].append([])
                            self.gameField[x][y][z] = 0
                            if x + y + z > 0 and random.randint(0, 10) == 0:
                                self.gameField[x][y][z] = 1
        hasRedBoxed = False
        for x in range(len(self.gameField)):
            self.fields.append([])
            for y in range(len(self.gameField[x])):
                self.fields[x].append([])
                for z in range(len(self.gameField[x][y])):
                    if self.gameField[x][y][z] == -1:
                        hasRedBoxed = True
                    self.fields[x][y].append(box (pos=vector(x, y, z), length = .8, height = .8, width = .8, color = color.blue, opacity = .1))
        self.fieldDimension = [x + 1, y + 1, z + 1]
        self.dim = 3 - self.fieldDimension.count(1)
        if not hasRedBoxed and self.dim > 1:
            n = 0
            random.seed(self.seed)
            while n < (self.fieldDimension[0] * self.fieldDimension[1] * self.fieldDimension[2]) / 10:
                pos = [random.randint(0, x), random.randint(0, y), random.randint(0, z)]
                if self.gameField[pos[0]][pos[1]][pos[2]] == 0 and sum(pos) > 0:
                    self.gameField[pos[0]][pos[1]][pos[2]] = -1
                    n = n + 1
        self.meeple = sphere(pos=vector(0, 0, 0), radius=0.45, color = color.yellow)
        scene.camera.follow(self.meeple)
        self.getNumOfBlueBoxes()
        time.sleep(1)

    def setRate(self, rate):
        self.rate = rate

    def saveCurrentMap(self, filename):
        with open(filename, "w") as fp:
            json.dump(self.gameField, fp)

    def getDimension(self):
        if self.dim == 1:
            return [self.fieldDimension[0]]
        elif self.dim == 2:
            return [self.fieldDimension[0], self.fieldDimension[1]]
        else:
            return self.fieldDimension

    def setCollecting(self, isCollecting):
        if self.isCollecting != isCollecting:
            self.isCollecting = isCollecting
            self.collectSwitches += 1
            if self.isCollecting:
                self.meeple.color = color.orange
            else:
                self.meeple.color = color.yellow

    def collect(self):
        self.gameField[int(self.meeple.pos.x)][int(self.meeple.pos.y)][int(self.meeple.pos.z)] = int(not self.gameField[int(self.meeple.pos.x)][int(self.meeple.pos.y)][int(self.meeple.pos.z)])
        self.collectSwitches += 2
        self.moves += 2
        return self.getNumOfBlueBoxes()

    def getNumOfBlueBoxes(self):
        sumBlueBoxes = 0
        for x in range(len(self.gameField)):
            for y in range(len(self.gameField[x])):
                for z in range(len(self.gameField[x][y])):
                    if self.gameField[x][y][z] == 0:
                        self.fields[x][y][z].color = color.blue
                        self.fields[x][y][z].opacity = .1
                    elif self.gameField[x][y][z] == 1:
                        self.fields[x][y][z].color = color.blue
                        sumBlueBoxes += 1
                        self.fields[x][y][z].opacity = 1
                    elif self.gameField[x][y][z] == -1:
                        self.fields[x][y][z].color = color.red
                        self.fields[x][y][z].opacity = 1
        if sumBlueBoxes == 0 and self.isRunning == True and self.callFinishAutomatically:
            self.finish()
        return sumBlueBoxes

    def moveTo(self, destination, isCollecting = -1):
        if self.isRunning:
            if (self.dim > 1 and (type(destination) is float or type(destination) is int)) or (type(destination) is list and self.dim > len(destination)):
                print("*** Warning: Length of destination paramter must equal Maze dimension! ***")
            if type(destination) is list:
                if len(destination) == 1:
                    destination = [destination[0], 0, 0]
                elif len(destination) == 2:
                    destination = [destination[0], destination[1], 0]
            elif type(destination) is int or type(destination) is float:
                destination = [destination, 0, 0]
            self.moves += 1
            if isCollecting >= 0 and self.isCollecting != isCollecting:
                self.isCollecting = isCollecting
                self.collectSwitches += 1
            if self.isCollecting:
                self.meeple.color = color.orange
            else:
                self.meeple.color = color.yellow
            destination = [min(max(0, int(destination[0])), self.fieldDimension[0] - 1), min(max(0, int(destination[1])), self.fieldDimension[1] - 1), min(max(0, int(destination[2])), self.fieldDimension[2] - 1)]
            if [abs(self.meeple.pos.x - destination[0]), abs(self.meeple.pos.y - destination[1]), abs(self.meeple.pos.z - destination[2])].count(0) >= 2:
                while self.meeple.pos.x != destination[0] or self.meeple.pos.y != destination[1] or self.meeple.pos.z != destination[2]:
                    delta = vector(np.sign(destination[0] - self.meeple.pos.x) / 10, np.sign(destination[1] - self.meeple.pos.y) / 10, np.sign(destination[2] - self.meeple.pos.z) / 10)
                    tmp = self.meeple.pos + (delta * 10)
                    if tmp.x < self.fieldDimension[0] and tmp.y < self.fieldDimension[1] and tmp.z < self.fieldDimension[2] and tmp.x >= 0 and tmp.y >= 0 and tmp.z >= 0:
                        if self.gameField[int(tmp.x)][int(tmp.y)][int(tmp.z)] == -1:
                            print("*** Warning: You hit a red box! ***")
                            break
                        else:
                            self.__meeple_animation(delta)
                            if self.isCollecting and self.gameField[int(self.meeple.pos.x)][int(self.meeple.pos.y)][int(self.meeple.pos.z)] == 0:
                                self.gameField[int(self.meeple.pos.x)][int(self.meeple.pos.y)][int(self.meeple.pos.z)] = 1
                            elif self.isCollecting and self.gameField[int(self.meeple.pos.x)][int(self.meeple.pos.y)][int(self.meeple.pos.z)] == 1:
                                self.gameField[int(self.meeple.pos.x)][int(self.meeple.pos.y)][int(self.meeple.pos.z)] = 0
                            self.getNumOfBlueBoxes()
                    else:
                        break
            else:
                print("*** Warning: Diagonal movement not allowed! ***")
        return self.getPosition()

    def moveX(self, steps = 1, isCollecting = -1):
        currentPos = self.getPosition(True)
        return self.moveTo([currentPos[0] + steps, currentPos[1], currentPos[2]], isCollecting)

    def moveY(self, steps = 1, isCollecting = -1):
        currentPos = self.getPosition(True)
        return self.moveTo([currentPos[0], currentPos[1] + steps, currentPos[2]], isCollecting)

    def moveZ(self, steps = 1, isCollecting = -1):
        currentPos = self.getPosition(True)
        return self.moveTo([currentPos[0], currentPos[1], currentPos[2] + steps], isCollecting)

    def getPosition(self, internalCall = False):
        if self.dim == 1 and internalCall == False:
            return [int(self.meeple.pos.x)]
        elif self.dim == 2 and internalCall == False:
            return [int(self.meeple.pos.x), int(self.meeple.pos.y)]
        else:
            return [int(self.meeple.pos.x), int(self.meeple.pos.y), int(self.meeple.pos.z)]

    def getFieldValue(self, destination = [-1, -1, -1]):
        if type(destination) is list and len(destination) == 3 and destination[0] == -1 and destination[1] == -1 and destination[2] == -1:
            if self.dim < 3:
                tmp = []
                if self.dim == 1:
                    for x in range(self.fieldDimension[0]):
                        tmp.append(self.gameField[x][0][0])
                    return tmp
                elif self.dim == 2:
                    for x in range(self.fieldDimension[0]):
                        tmp.append([])
                        for y in range(self.fieldDimension[1]):
                            tmp[x].append(self.gameField[x][y][0])
                    return tmp
            return self.gameField
        else:
            if (self.dim > 1 and (type(destination) is float or type(destination) is int)) or (type(destination) is list and self.dim > len(destination)):
                print("*** Warning: Length of destination paramter must equal Maze dimension! ***")
            if type(destination) is list:
                if len(destination) == 1:
                    destination = [destination[0], 0, 0]
                elif len(destination) == 2:
                    destination = [destination[0], destination[1], 0]
            elif type(destination) is int or type(destination) is float:
                destination = [destination, 0, 0]
            
            returnList = []
            if not type(destination[0]) is list:
                destination[0] = [destination[0]]
            if not type(destination[1]) is list:
                destination[1] = [destination[1]]
            if not type(destination[2]) is list:
                destination[2] = [destination[2]]
            for x in range(len(destination[0])):
                for y in range(len(destination[1])):
                    for z in range(len(destination[2])):
                        destination[0][x] = int(destination[0][x])
                        destination[1][y] = int(destination[1][y])
                        destination[2][z] = int(destination[2][z])
                        if destination[0][x] >= 0 and destination[1][y] >= 0 and destination[2][z] >= 0 and destination[0][x] < self.fieldDimension[0] and destination[1][y] < self.fieldDimension[1] and destination[2][z] < self.fieldDimension[2]:
                            returnList.append(self.gameField[destination[0][x]][destination[1][y]][destination[2][z]])
                            if self.gameField[destination[0][x]][destination[1][y]][destination[2][z]] == -1:
                                return returnList
                        else:
                            returnList.append(-1)
                            return returnList
            return returnList
    
    def getFieldValueX(self, direction):
        return self.getFieldValue([[np.sign(direction) * (x + 1) + self.meeple.pos.x for x in list(range(0, abs(direction)))], self.meeple.pos.y, self.meeple.pos.z])

    def getFieldValueY(self, direction):
        return self.getFieldValue([self.meeple.pos.x, [np.sign(direction) * (x + 1) + self.meeple.pos.y for x in list(range(0, abs(direction)))], self.meeple.pos.z])

    def getFieldValueZ(self, direction):
        return self.getFieldValue([self.meeple.pos.x, self.meeple.pos.y, [np.sign(direction) * (x + 1) + self.meeple.pos.z for x in list(range(0, abs(direction)))]])

    def finish(self):
        self.isRunning = False
        score = self.moves + self.collectSwitches + 10*self.getNumOfBlueBoxes()
        print("Result:")
        print("\tMoves:\t\t\t%d" % (self.moves))
        print("\tCollection switches:\t%d" % (self.collectSwitches))
        print("\tMissed Boxes:\t\t%d" % (self.getNumOfBlueBoxes()))
        print("\tTotal score:\t\t%d" % score)
        stop_server()
        #exit()

    def __meeple_animation(self, delta):
        loop = True
        while loop:
            rate(self.rate)
            self.meeple.pos = vector(round((self.meeple.pos.x + delta.x) * 10) / 10, round((self.meeple.pos.y + delta.y) * 10) / 10, round((self.meeple.pos.z + delta.z) * 10) / 10)
            if self.meeple.pos.x.is_integer() and self.meeple.pos.y.is_integer() and self.meeple.pos.z.is_integer():
                loop = False

if __name__ == "__main__":
    x = MeepleMaze("random", size = [10, 5, 3])
    x.finish()

# --------------------Licence ---------------------------------------------
# Copyright (c) <2021> S. Franz and S. Kissner
# Jade University of Applied Sciences
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject
# to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# eof