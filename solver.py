

from textwrap import dedent
import meeplemaze


GAME_MODE_DEFAULT = 1
GAME_MODE_REPLAY = 2


def read_user_input():
    """Explain the game to the user and let him choose the game play mode
       (solve unknown maze or replay a already solved maze):

       Mode 1: The user can create a board using custom values or use default values,
               returns the game play mode and the game board as result

       Mode 2: The user can select a map for replay using a filename
               (the corresponding tracking_path file is automatically determined if the naming conventions are met),
               return the game play mode, the loaded map/ board and the 'tracking_path' as result"""

    while True:
        print_dedent("""
        Hello and welcome to MeepleMaze :)
        At first you have to decide which mode you want to play.

        You have two options to choose from:
        The first option is to create a board and start the game.
        The second option is to provide a file_name for an already played map (and tracking path),
        then I will move and collect boxes like according to the movements in the tracking path.
        If you want to play option one, type "1":
        If you want to play option two, type "2":""")

        play_mode = int(input())
        if play_mode == GAME_MODE_DEFAULT:
            print_dedent("""
            Good choice! Now we have to create a board.
            If you want to use default values (size = 8), press [Enter].""")

            len_x_axis = input('Please choose the length of the x-axis, min: 3 ')
            len_y_axis = input('Please choose the length of the y-axis, min: 3 ')
            if len_x_axis == '' and len_y_axis == '':
                size = [8, 8]
            elif int(len_x_axis) >= 3 and int(len_y_axis) >= 3:
                size = [int(len_x_axis), int(len_y_axis)]
            else:
                print('Input error, please try again.')
                continue                # ValueError: 'a' , bei -4 hängt er sich auf

            print('How fast should the game run? (10 = slow, 500 = extreme fast) ')
            rate_input = input('If you want to use the default speed (50), press [Enter]. ')
            if rate_input == '':
                rate = 50
            elif int(rate_input) > 0:
                rate = int(rate_input)
            else:
                print('Input error, please try again')
                continue

            seed_input = input("Which board do you want to play? Input a number, otherwise I'll take a random board: ")
            if seed_input == '':
                seed = -1
            elif int(seed_input) > 0:
                seed = int(seed_input)
            else:
                print('Input error, please try again.')
                continue

            game = meeplemaze.MeepleMaze("random", size, seed, rate, callFinishAutomatically=False)
            return play_mode, game, None

        elif play_mode == GAME_MODE_REPLAY:
            print_dedent("""
            Good choice! Now we have to look for a file with an already solved map.
            Tell me the name of your favorite map file and ensure,
            that the map and tracking_path file are in the current directory.
            Furthermore, ensure that the tracking path file has the same date suffix as the map file,
            e.g. 'map_18_12_2021_18_36_58' and 'tracking_path_18_12_2021_18_36_58'""")

            try:
                map_name = input('For example: map_18_12_2021_18_36_58 : \n')
                date_suffix = map_name[-19:]
                tracking_path = f'tracking_path_{date_suffix}'

                game = meeplemaze.MeepleMaze(map_name, callFinishAutomatically=False)
                return play_mode, game, tracking_path
            except UnboundLocalError:
                print('Wrong input, please try again.')
                continue

        else:
            print('Wrong input, please try again.')
            continue


def print_dedent(text):
    """Print the text into a readable format."""
    print(dedent(text))

