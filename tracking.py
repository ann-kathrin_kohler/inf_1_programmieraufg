# --------------------Licence ---------------------------------------------
# Copyright (c) <2022>
# - A. Kohler (ann-kathrin.kohler@student.jade-hs.de) and
# - L. Stoffel (leon.stoffel@student.jade-hs.de)
# Jade University of Applied Sciences
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject
# to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -------------------------------------------------------------------------


"""This module provides various functions to save the 'tracking_path'
   to a file, load and replay 'tracking_path' from a given file."""


import movements


def save_tracking_path_to_file(tracking_path, file_name):
    """Create a new file named 'file_name' and save 'tracking_path' to file."""
    with open(file_name, 'w') as f:
        for breadcrumb in tracking_path:
            f.write(breadcrumb + '\n')


def load_tracking_path_from_file(file_name):
    """Load a 'tracking_path' from a file named 'file_name'."""
    tracking_path = []
    with open(file_name, 'r') as f:
        file_content = f.read()
        lines = file_content.split('\n')
        for line in lines:
            if line != '':
                tracking_path.append(line)
    return tracking_path


def move_along_tracking_path(game, tracking_path):
    """Replay movements and collect operations according to the provided 'tracking_path'."""
    for breadcrumb in tracking_path:
        movement = breadcrumb[:2]
        steps = int(breadcrumb[2:])

        if movement == 'ME':
            movements.move_east(game, steps)
        elif movement == 'MW':
            movements.move_west(game, steps)
        elif movement == 'MN':
            movements.move_north(game, steps)
        elif movement == 'MS':
            movements.move_south(game, steps)
        elif movement == 'CE':
            movements.collect_east(game, steps)
        elif movement == 'CW':
            movements.collect_west(game, steps)
        elif movement == 'CN':
            movements.collect_north(game, steps)
        elif movement == 'CS':
            movements.collect_south(game, steps)
